package net.cureau.prometheus.component

import net.cureau.prometheus.component.exceptions.AddressOutOfBoundsException
import net.cureau.prometheus.component.primitives.MemoryTrait

/**
 * Implements an 8-bit memory.
 * Values from 0x0 - 0xFF
 */
class Memory implements MemoryTrait {

    Memory(int startAddress, int size) {
        this.startAddress = startAddress
        this.size = size
        this.content = new int[size]
    }

    @Override
    int read(int address) {
        if (inRange(address)) {
            content[address - startAddress] & 0xFF
        } else {
            throw new AddressOutOfBoundsException('Attempt to read address ${address} which is not in range of this device')
        }
    }

    @Override
    void write(int address, int data) {
        if (readOnly) {
            throw new Exception('Attempt to write to a read-only device')
        }
        if (inRange(address)) {
            content[address - startAddress] = data & 0xFF
        } else {
            throw new AddressOutOfBoundsException('Attempt to write address ${address} which is not in range of this device')
        }
    }
}
