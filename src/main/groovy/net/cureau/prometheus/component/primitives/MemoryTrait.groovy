package net.cureau.prometheus.component.primitives

trait MemoryTrait {
    int startAddress
    int size
    boolean readOnly
    int[] content

    void setStartAddress(int startAddress) {
        this.startAddress = startAddress
    }

    void setSize(int size) {
        this.size = size
    }

    boolean inRange(int address) {
        int endAddress = (startAddress + size) - 1
        if ((address >= startAddress) && (address <= endAddress)) {
            return true
        }
        false
    }

    /**
     * Fill all memory with data
     * @param data value to write to all locations
     */
    void fill(int data) {
        Arrays.fill(content, data)
    }

    /**
     * Clear contents from memory
     */
    void clear() {
        fill(0x00)
    }

    /**
     * Once a memory has been marked read only, it stays that way
     * TODO: Determine if this really happens in the wild
     */
    void setReadOnly() {
        this.readOnly = true
    }

    boolean getReadOnly() {
        this.readOnly
    }

    abstract int read(int address)
    abstract void write(int address, int data)
}