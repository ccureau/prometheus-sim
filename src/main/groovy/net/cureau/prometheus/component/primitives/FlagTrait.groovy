package net.cureau.prometheus.component.primitives

/**
 * Flags are a primitive bit type. It can be on or off.
 */
trait FlagTrait {
    /**
     * The state for this flag
     */
    private boolean state;

    /**
     * Set the flag
     */
    void set() {
        this.state = true
    }

    /**
     * Clear the flag
     */
    void clear() {
        this.state = false
    }

    /**
     * Query the flag state
     * @return true if the flag is set, false if it is clear
     */
    boolean isSet() {
        this.state
    }
}
