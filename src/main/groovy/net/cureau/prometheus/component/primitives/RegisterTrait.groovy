package net.cureau.prometheus.component.primitives

/**
 * Trait for a Register
 * A Register object can be read from and written to, but needs its scope limited to the CPU.
 * Registers for different CPUs will need different abilities, so this base class allows extension.
 */
trait RegisterTrait {
    private int value

    /**
     * Get the current value of the register
     * @return The current value of the register
     */
    int getValue() {
        this.value
    }

    /**
     * Set the value for this register
     * @param value The value the register should be set to
     */
    void setValue(int value) {
        this.value = value
    }
}