package net.cureau.prometheus.component.exceptions

class AddressOutOfBoundsException extends RuntimeException {
    AddressOutOfBoundsException(String errorMessage) {
        super(errorMessage);
    }
}
