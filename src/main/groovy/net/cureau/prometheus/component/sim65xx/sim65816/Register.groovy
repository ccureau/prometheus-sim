package net.cureau.prometheus.component.sim65xx.sim65816

import net.cureau.prometheus.component.primitives.RegisterTrait

/**
 * Implementation of a 65816 Register
 *
 * Registers must be able to be switched from 8-bit (emulation) to 16-bit (native) mode
 * and back again at will.
 */
class Register implements RegisterTrait {
    boolean emulation

    /**
     * Constructor for a 65816 style Register
     * All 65816 registers start in emulation mode (8-bit)
     */
    Register() {
        this.emulation = true
    }

    int get() {
        if (emulation) {
            getValue() & 0xFF
        } else {
            getValue() & 0xFFFF
        }
    }

    void set(int value) {
        if (emulation) {
            setValue(highByte() + (value & 0xFF))
        } else {
            setValue(value & 0xFFFF)
        }
    }

    int highByte() {
        getValue() >> 8
    }

    int lowByte() {
        getValue() & 0xFF
    }

    void increment() {
        if (emulation) {
            setValue((highByte() << 8) + ((lowByte() + 1) & 0xFF))
        } else {
            setValue((getValue() + 1) & 0xFFFF)
        }
    }

    void decrement() {
        if (emulation) {
            setValue((highByte() << 8) + ((lowByte() - 1) & 0xFF))
        } else {
            setValue((getValue() - 1) & 0xFFFF)
        }
    }
}