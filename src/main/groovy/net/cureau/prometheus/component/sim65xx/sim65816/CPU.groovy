package net.cureau.prometheus.component.sim65xx.sim65816

/**
 * The CPU encompasses all of the different components into one unit.
 */
class CPU {
    /**
     * Registers inside of the 65816
     */
    Register a
    Register x
    Register y
    Register sp
    Register pc

    /**
     * Status Register
     */
    StatusFlags flags

    CPU() {
        reset()
    }

    void reset() {
        /**
         * Registers (set to emulation mode on reset)
         */
        a = new Register()
        x = new Register()
        y = new Register()

        /**
         * Stack Pointer and Program Counter are always 16-bit
         */
        sp = new Register()
        sp.emulation = false
        pc = new Register()
        pc.emulation = false

        /**
         * Status flags (set to emulation mode on reset)
         */
        flags = new StatusFlags()
        flags.setEmulation()
    }
}
