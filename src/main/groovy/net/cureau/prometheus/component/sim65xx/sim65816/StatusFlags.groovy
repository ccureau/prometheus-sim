package net.cureau.prometheus.component.sim65xx.sim65816

class StatusFlags {

    /**
     * Status flags in native mode:
     * 7 6 5 4 3 2 1 0
     * n v m b d i z c
     *               e (emulation mode can only be toggled via carry flag)
     *
     * Status flags in emulation mode:
     * 7 6 5 4 3 2 1 0
     * n v - b d i z c (bit 5 is always 0)
     */

    Flag n
    Flag v
    Flag m
    Flag bx
    Flag d
    Flag i
    Flag z
    Flag c
    Flag e

    StatusFlags() {
        this.n = new Flag()
        this.v = new Flag()
        this.m = new Flag()
        this.bx = new Flag()
        this.d = new Flag()
        this.i = new Flag()
        this.z = new Flag()
        this.c = new Flag()
        this.e = new Flag()
    }

    boolean emulation() {
        this.e.isSet()
    }

    void setEmulation() {
        this.e.set()
        this.m.clear()
        this.m.setImmutable()
    }

    void clearEmulation() {
        this.e.clear()
        this.m.clearImmutable()
    }

    int getValue() {
        int value = 0

        value += (this.n.isSet() ? 1 << 7 : 0) +
                (this.v.isSet() ? 1 << 6 : 0) +
                (emulation() ? (0) : (m.isSet() ? 1 << 5 : 0)) +
                (this.bx.isSet() ? 1 << 4 : 0) +
                (this.d.isSet() ? 1 << 3 : 0) +
                (this.i.isSet() ? 1 << 2 : 0) +
                (this.z.isSet() ? 1 << 1 : 0) +
                (this.c.isSet() ? 1 : 0)

        value & 0xFF
    }

    void setValue(int value) {
        (value & (1 << 7)) ? n.set() : n.clear()
        (value & (1 << 6)) ? v.set() : v.clear()
        emulation() ? null : ((value & (1 << 5))? m.set() : m.clear())
        (value & (1 << 4)) ? bx.set() : bx.clear()
        (value & (1 << 3)) ? d.set() : d.clear()
        (value & (1 << 2)) ? i.set() : i.clear()
        (value & (1 << 1)) ? z.set() : z.clear()
        (value & (1 << 0)) ? c.set() : c.clear()
    }

    List getFlags() {
        List value = []
        n.isSet() ? value << 'N' : null
        v.isSet() ? value << 'V' : null
        emulation() ? null : (m.isSet() ? value << 'M' : null)
        bx.isSet() ? (emulation() ? value << 'B' : value << 'X') : null
        d.isSet() ? value << 'D' : null
        i.isSet() ? value << 'I' : null
        z.isSet() ? value << 'Z' : null
        c.isSet() ? value << 'C' : null
        e.isSet() ? value << 'E' : null
        value
    }
}
