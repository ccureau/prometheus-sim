package net.cureau.prometheus.component.sim65xx.sim65816


import net.cureau.prometheus.component.sim65xx.sim65816.traits.Sim65816Flag
/**
 * Implements a 65816 Flag
 */
class Flag implements Sim65816Flag {

    /**
     * Flags always start as cleared and mutable
     */
    Flag() {
        clear()
        clearImmutable()
    }
}
