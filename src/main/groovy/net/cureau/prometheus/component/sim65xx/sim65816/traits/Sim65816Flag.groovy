package net.cureau.prometheus.component.sim65xx.sim65816.traits

import net.cureau.prometheus.component.primitives.FlagTrait

/**
 * Traits specific to the 65816 implementation of a flag
 */
trait Sim65816Flag extends FlagTrait {

    private immutable

    /**
     * Set this flag as immutable
     */
    void setImmutable() {
        this.immutable = true
    }

    /**
     * Set this flag as mutable
     */
    void clearImmutable() {
        this.immutable = false
    }

    /**
     * Determine if this flag is mutable or not
     * @return True if mutable, False if immutable
     */
    boolean isImmutable() {
        this.immutable
    }

    /**
     * Set iff flag is mutable
     */
    @Override
    void set() {
        if (! immutable) {
            super.set()
        }
    }

    /**
     * Clear iff flag is mutable
     */
    @Override
    void clear() {
        if (! immutable) {
            super.clear()
        }
    }
}