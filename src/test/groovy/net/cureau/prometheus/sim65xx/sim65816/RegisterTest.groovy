package net.cureau.prometheus.sim65xx.sim65816

import net.cureau.prometheus.component.sim65xx.sim65816.Register
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

import static org.junit.jupiter.api.Assertions.assertEquals
import static org.junit.jupiter.api.Assertions.assertTrue

class RegisterTest {

    Register r

    @BeforeEach
    void standUp() {
        this.r = new Register()
    }

    @AfterEach
    void tearDown() {
        this.r = null
    }

    @Test
    @DisplayName('Test basic native mode function')
    void testNativeModeBasicFunction() {
        r.emulation = false
        r.set(0xABCD)
        assertEquals(0xABCD, r.get(), 'Ensure 16-bit get()')
        assertEquals(0xAB, r.highByte(), 'highByte() returns 0xAB')
        assertEquals(0xCD, r.lowByte(), 'lowByte() returns 0xCD')
    }

    @Test
    @DisplayName('Native mode: increment')
    void testNativeModeIncrement() {
        r.emulation = false
        r.set(0xABFF)
        r.increment()
        assertEquals(0xAC00, r.get(), 'Overflow of low byte affects high byte')
        r.set(0xFFFF)
        r.increment()
        assertEquals(0x0, r.get(), 'Overflow of high byte affects low byte')
    }

    @Test
    @DisplayName('Native mode: decrement')
    void testNativeModeDecrement() {
        r.emulation = false
        r.set(0xAB00)
        r.decrement()
        assertEquals(0xAAFF, r.get(), 'Underflow of low byte affects high byte')
        r.set(0x0)
        r.decrement()
        assertEquals(0xFFFF, r.get(), 'Underflow of high byte affects low byte')
    }

    @Test
    @DisplayName('Test basic emulation mode function')
    void testEmulationModeBasicFunction() {
        assertTrue(r.emulation, 'Register should be in emulation mode on instantiation')
        r.set(0xABCD)
        assertEquals(0xCD, r.get(), 'Ensure 8-bit get()')
        assertEquals(0x00, r.highByte(), 'highByte() returns 0x00')
        assertEquals(0xCD, r.lowByte(), 'lowByte() returns 0xCD')
    }

    @Test
    @DisplayName('Native mode: increment')
    void testEmulationModeIncrement() {
        r.emulation = false
        r.set(0xABFF)

        r.emulation = true
        r.increment()
        assertEquals(0x00, r.get(), 'Overflow resets byte')
        r.emulation = false
        assertEquals(0xAB00, r.get(), 'Overflow of low byte does not affect high byte')

        r.set(0xFFFF)
        r.emulation = true
        r.increment()
        assertEquals(0x00, r.get(), 'Overflow resets byte')
        r.emulation = false
        assertEquals(0xFF00, r.get(), 'Overflow of high byte does not affect low byte')
    }

    @Test
    @DisplayName('Emulation mode: decrement')
    void testEmulationModeDecrement() {
        r.emulation = false
        r.set(0xAB00)

        r.emulation = true
        r.decrement()
        assertEquals(0xFF, r.get(), 'Underflow resets byte')
        r.emulation = false
        assertEquals(0xABFF, r.get(), 'Underflow of low byte does not affect high byte')

        r.set(0x0000)
        r.emulation = true
        r.decrement()
        assertEquals(0xFF, r.get(), 'Underflow resets byte')
        r.emulation = false
        assertEquals(0xFF, r.get(), 'Underflow of high byte does not affect low byte')

    }
}
