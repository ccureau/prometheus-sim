package net.cureau.prometheus.sim65xx.sim65816


import net.cureau.prometheus.component.sim65xx.sim65816.StatusFlags
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import static org.junit.jupiter.api.Assertions.assertEquals
import static org.junit.jupiter.api.Assertions.assertFalse
import static org.junit.jupiter.api.Assertions.assertTrue

class StatusFlagsTest {
    StatusFlags status

    @BeforeEach
    void standUp() {
        this.status = new StatusFlags()
    }

    @AfterEach
    void tearDown() {
        this.status = null
    }

    @Test
    void testConstructor() {
        assertFalse(status.e.isSet(), 'Constructor sets emulation mode')
        assertEquals(0x00, status.getValue(), 'All flags start off cleared')
    }

    @Test
    void testEmulationMode() {
        status.setEmulation()
        assertTrue(status.emulation(), 'Status Flags should be in emulation mode')
        status.clearEmulation()
        assertFalse(status.emulation(), 'Status Flags should be in native mode')
    }

    @Test
    void testImmutableFlag() {
        status.setEmulation()
        status.m.set()
        assertFalse(status.m.isSet(), 'Immutable flag should never be set')
        status.clearEmulation()
        status.m.set()
        assertTrue(status.m.isSet(), 'Change from immutable to mutable should allow flag to be set')
    }

    @Test
    void testSetFlags() {
        status.c.set()
        status.z.set()
        status.d.set()
        assertEquals(0x0B, status.getValue(), 'Emulation mode: Flags C, Z, D set')
    }

    @Test
    void testSetValue() {
        status.setEmulation()
        status.setValue(0xFF)
        assertEquals(0xDF, status.getValue(), 'm flag is immutable in emulation mode')

        status.clearEmulation()
        status.setValue(0xFF)
        assertEquals(0xFF, status.getValue(), 'm flag is mutable in native mode')
    }

    @Test
    void testEmulationToString() {
        // n v - b d i z c
        List flags

        status.setEmulation()

        status.setValue(0xFF)
        flags = status.getFlags()
        assertTrue(flags.containsAll(['N','V','B','D','I','Z','C']), 'All flags should be set')
        status.setValue(0x0)
        flags = status.getFlags()
        assertFalse(flags.containsAll(['N','V','B','D','I','Z','C']), 'All flags should be cleared')
        status.setValue(0b10101010)
        flags = status.getFlags()
        assertTrue(flags.containsAll(['N','D','Z']), 'N, D, Z flags set')
        assertFalse(flags.contains('M'), 'M flag not set in emulation mode')
        assertFalse(flags.contains('V'), 'V flag should not be set')
        assertFalse(flags.contains('B'), 'B flag should not be set')
        assertFalse(flags.contains('I'), 'I flag should not be set')
        assertFalse(flags.contains('C'), 'C flag should not be set')
    }

    @Test
    void testNativeToString() {
        // n v m x d i z c
        List flags

        status.clearEmulation()
        status.setValue(0xFF)
        flags = status.getFlags()
        assertTrue(flags.containsAll(['N','V','M','X','D','I','Z','C']), 'All flags should be set')
        status.setValue(0x0)
        flags = status.getFlags()
        assertFalse(flags.containsAll(['N','V','M','X','D','I','Z','C']), 'All flags should be cleared')
        status.setValue(0b10111010)
        flags = status.getFlags()
        assertTrue(flags.containsAll(['N', 'M', 'D', 'X', 'Z']), 'N, M, D, X, Z flags set')
        assertFalse(flags.contains('V'), 'V flag should not be set')
        assertFalse(flags.contains('I'), 'I flag should not be set')
        assertFalse(flags.contains('C'), 'C flag should not be set')
    }

}
